import React, { useState } from 'react';
import { View, Text, Button, TextInput, Switch } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons'

import AppButton from './app/components/AppButton';
// import ViewImageScreen from './app/screens/ViewImageScreen';
import WelcomeScreen from './app/screens/WelcomeScreen';
import Card from './app/components/Card';
import ListingDetailsScreen from './app/screens/ListingDetailsScreen';
import ListItem from './app/components/ListItem';
import ViewImageScreen from './app/screens/ViewImageScreen';
import MessagesScreen from './app/screens/MessagesScreen';
import Icon from './app/components/Icon';
import Screen from './app/components/Screen';
import AccountScreen from './app/screens/AccountScreen';
import ListingsScreen from './app/screens/ListingsScreen';
import AuthNavigator from './app/navigation/AuthNavigator';
import AppTextInput from './app/components/AppTextInput';
import AppPicker from './app/components/AppPicker';
import LoginScreen from './app/screens/LoginScreen';
import ListingEditScreen from './app/screens/ListingEditScreen';
import navigationTheme from './app/navigation/navigationTheme';
import AppNavigator from './app/navigation/AppNavigator';

// const Link = () => {
//   const navigation = useNavigation();

//   return (
//     <Button 
//       title="Click link" 
//       onPress={() => navigation.navigate("TweetDetails")} />
//   );
// }

// const Tweets = ({ navigation }) => (
//   <Screen>
//     <Text>Tweets</Text>
//     {/* <Link /> */}
//     <Button 
//       title="View Tweet"
//       onPress={ () => navigation.navigate("TweetDetails" , { id: 1}) } />
//   </Screen>
// );

// const TweetDetails = ({ route }) => (
//   <Screen>
//     <Text>Tweet details {route.params.id} </Text>
//   </Screen>
// );

// const Stack = createStackNavigator();

// const FeedNavigator = () => (
//   <Stack.Navigator
//     screenOptions={{
//       headerStyle: { backgroundColor: 'tomato'},
//       headerTintColor: 'white',
//     }}>
//     <Stack.Screen 
//       name="Tweets" 
//       component={Tweets} 
//       options={{
//         headerShown: false,
//       }} />
//     <Stack.Screen 
//       name="TweetDetails" 
//       component={TweetDetails} 
//       options={({ route }) => ({
//         title: route.params.id,
//       })} />
//   </Stack.Navigator>
// );

// const AccountNavigator = () => (
//   <Screen>
//     <Text>Account</Text>
//   </Screen>
// )

// const Tab = createBottomTabNavigator();
// const TabNavigator = () => (
//   <Tab.Navigator
//     tabBarOptions={{
//       activeBackgroundColor: 'red',
//       activeTintColor: 'white',
//       inactiveBackgroundColor: '#eee',
//       inactiveTintColor: 'black',
//     }}>
//     <Tab.Screen 
//     name="Feed" 
//     component={FeedNavigator}
//     options={{
//       tabBarIcon: ({ size, color }) => (
//         <MaterialCommunityIcons name='home' size={size} color={color} />
//       )
//     }} /> 
//     <Tab.Screen name="Account" component={AccountNavigator} />
//   </Tab.Navigator>
// );



export default function App() {

  return (

    <NavigationContainer theme={navigationTheme}>
      <AppNavigator />
    </NavigationContainer>

    // <ListingEditScreen />

    // <LoginScreen />

    // <Screen style={{backgroundColor: '#fff'}} >

    //   <AppPicker 
    //     icon="apps" 
    //     items={categories} 
    //     onSelectItem={item => setCategory(item)}
    //     placeholder="Category" 
    //     selectedItem={category}
    //   />

    //   <AppTextInput icon="email" placeholder="Email" />

    //   {/* <AppTextInput
    //     placeholder="Username"
    //     icon="email"
    //   /> */}

    //   {/* <Text>{isNew ? 'ok' : 'no'}</Text>
    //   <Switch value={isNew} onValueChange={(newValue) => setIsNew(newValue)} /> */}
      
    // </Screen>



    // <WelcomeScreen />
    // <Card title="title" subTitle="subtitle" image={require("./app/assets/logo-red.png")} />
    // <ListingDetailsScreen />
    // <ListItem title="title" subTitle="sub" image={require("./app/assets/mosh.jpg")} />
    // <ViewImageScreen />
    // <MessagesScreen />
    // <Icon name="email" size={88} />
    // <ListItem title="My title" ImageComponent={<Icon name="email" />} />
    // <AccountScreen />
    // <ListingsScreen />





  );

}
