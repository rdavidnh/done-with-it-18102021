import React from "react";
import { FlatList, StyleSheet } from "react-native";

import Card from "../components/Card";
import Screen from "../components/Screen";
import colors from "../config/colors";

const listings = [
  {
    id: 1,
    title: 'Red jacket',
    price: 100,
    image: require('../assets/jacket.jpg'),
  },
  {
    id: 2,
    title: 'Couch',
    price: 200,
    image: require('../assets/couch.jpg'),
  }
];

export default ({ navigation }) => (
  <Screen style={styles.screen} >
    <FlatList 
      data={listings} 
      keyExtractor={listings => listings.id.toString()} 
      renderItem={({item}) =>
        <Card 
          title={item.title}
          subTitle={"$" + item.price}
          image={item.image}
          onPress={() => navigation.navigate("ListingDetails", item) }
        />
      } 
    />
  </Screen>
)

const styles = StyleSheet.create({
  screen: {
    paddingHorizontal: 5,
    backgroundColor: colors.light,
  }
})
