import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as Yup from "yup";

import { AppForm, AppFormField, AppFormPicker, SubmitButton } from '../components/forms';
import Screen from "../components/Screen";

const validationSchema = Yup.object().shape({
    title: Yup.string().required().min(1).label("Title"),
    price: Yup.number().required().min(1).max(5000).label("Price"),
    description: Yup.string().required().label("Description"),
    category: Yup.object().required().nullable().label("Category"),
});

const categories = [
    { label: "Furniture", value: 1 },
    { label: "Clothing", value: 2 },
    { label: "Cameras", value: 3 },
];

export default function ListingEditScreen() {
    return (
        <Screen style={styles.container}>
            <AppForm 
                initialValues={{
                    title: "",
                    price: "",
                    description: "",
                    category: null,
                }}
                onSubmit={(value) => console.log(value)}
                validationSchema={validationSchema}
            >
                <AppFormField 
                    maxLength={255} name="title" placeholder="Title"
                />
                <AppFormField 
                    keyboardType="numeric"
                    maxLength={8}
                    name="price"
                    placeholder="Price"
                />
                <AppFormPicker 
                    items={categories}
                    name="category"
                    placeholder="Category"
                />
                <AppFormField 
                    height={80}
                    maxLength={255}
                    multiline={true}
                    name="description"
                    numberOfLines={5}
                    placeholder="Description"
                />
                <SubmitButton title="Login" />
            </AppForm>
        </Screen>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
    }
});
