import React, { Children } from 'react';
import { SafeAreaView, View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';

import colors from '../config/colors';

export default function Screen({children, style}) {
    return (
        <SafeAreaView style={[styles.screen, style]}>
            <View style={style}>
                {children}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    screen: {
        paddingTop: Constants.statusBarHeight,
        // backgroundColor: colors.light,
        flex: 1,
    }
});
